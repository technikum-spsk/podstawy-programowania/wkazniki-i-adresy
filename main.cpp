#include <iostream>

using namespace std;

int main() {
    int table[2][2] = {
            {0, 1},
            {2, 3}
    };

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            cout << "table[" << i << "][" << j << "]: " << table[i][j] << endl;
        }
    }

    cout << endl;

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            cout << "&table[" << i << "][" << j << "]: " << &table[i][j] << endl;
        }
    }

    cout << endl;

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            cout << "*(table + " << i << ") + " << j << ": " << *(table + i) + j << endl;
        }
    }

    cout << endl;

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            cout << "*(*(table + " << i << ") + " << j << "): " << *(*(table + i) + j) << endl;
        }
    }

    cout << endl;

    cout << "&table[1][1]: " << &table[1][1] << endl;
    cout << "table[1][1]: " << table[1][1] << endl;
    cout << "&table[1][0] + 1: " << &table[1][0] + 1 << endl;
    cout << "table[1][0] + 1: " << table[1][0] + 1 << endl;

    cout << endl;

    cout << &table[0][0] << endl;
    cout << *table << endl;
    cout << *(*(table)) << endl;
    cout << &table[0][1] << endl;
    cout << *table + 1 << endl;
    cout << *(*(table) + 1) << endl;
    cout << &table[1][0] << endl;
    cout << *(table + 1) << endl;
    cout << *(*(table + 1)) << endl;
    cout << &table[1][1] << endl;
    cout << *(table + 1) + 1 << endl;
    cout << *(*(table + 1) + 1) << endl;

    return 0;
}
